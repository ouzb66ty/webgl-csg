import { BoxGeometry, Color, Mesh, MeshPhysicalMaterial } from 'three';

/* BoxGeometry solid object example */

export class Brick extends Mesh {
  constructor(size: number, color: Color) {
    super();
    this.geometry = new BoxGeometry(size, size, size);
    this.material = new MeshPhysicalMaterial({ color });
  }
}
