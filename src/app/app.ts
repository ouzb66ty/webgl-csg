import { Color, HemisphereLight, PerspectiveCamera, Scene, Vector3, WebGLRenderer } from 'three';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader';
import { CSG } from 'three-csg-ts';
import { STLSolid } from './STLSolid';
import { OrbitControls } from '@three-ts/orbit-controls';

export class App {
  private readonly scene = new Scene();
  private readonly camera = new PerspectiveCamera(45, innerWidth / innerHeight, 0.1, 10000);
  private readonly renderer = new WebGLRenderer({
    antialias: true,
    canvas: document.getElementById('main-canvas') as HTMLCanvasElement,
  });
  private readonly loader: STLLoader;
  private readonly controls: OrbitControls;

  private brickSTL: STLSolid;
  private distalSTL2: STLSolid;
  private light: HemisphereLight;

  constructor() {
    this.loader = new STLLoader();
    this.brickSTL = new STLSolid(this.loader, 'http://127.0.0.1:9000/dist/stl/cube.stl');
    this.distalSTL2 = new STLSolid(this.loader, 'http://127.0.0.1:9000/dist/stl/distal_decimated.stl');
    this.light = new HemisphereLight(0xffffbb, 0x080820, 1);
    this.controls = new OrbitControls(this.camera, this.renderer.domElement );

    this.camera.position.set(200, 200, 200);
    this.camera.lookAt(new Vector3(0, 0, 0));

    this.renderer.setSize(innerWidth, innerHeight);
    this.renderer.setClearColor(new Color('rgb(0,0,0)'));

    this.controls.update();
  }

  public run(){
    this.brickSTL.emitter.on('loaded', () => {
      this.distalSTL2.emitter.on('loaded', () => {

        this.brickSTL.updateMatrix();
        this.distalSTL2.updateMatrix();

        const resultUnion = CSG.union(this.brickSTL, this.distalSTL2);
        const resultIntersection = CSG.intersect(this.brickSTL, this.distalSTL2);
        const resultSubtract = CSG.subtract(this.brickSTL, this.distalSTL2);

        this.scene.add(this.light);
        this.scene.add(resultUnion);
        resultIntersection.position.z += 100;
        this.scene.add(resultIntersection);
        resultSubtract.position.z -= 100;
        resultSubtract.position.y -= 20;
        this.scene.add(resultSubtract);

        this.render();

      });
    });
  }

  private adjustCanvasSize() {
    this.renderer.setSize(innerWidth, innerHeight);
    this.camera.aspect = innerWidth / innerHeight;
    this.camera.updateProjectionMatrix();
  }

  private render() {
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(() => this.render());
    this.adjustCanvasSize();
  }
}
