# WebGL CSG

Small webgl application performing the 3 CSG operations with a cube in order :
1. Subtraction
2. Union
3. Intersection

The two linked parts are imported STL files. It is possible to perform an orbital control with the mouse and the keyboard.
I focused on the quality and maintainability of the code with the latest versions.

![](demo.png)

- Typescript
- ES6
- Three.JS (r132), three-csg-ts and OrbitControls
- Webpack
- onfire.js for EventEmitter

## Installing dependencies

    npm i

## Running the project in watch mode

    npm start

## Building the project

    npm run build

## Building the project (dev mode)

    npm run dev-build

## Linting the code

    npm run lint